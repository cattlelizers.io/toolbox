# Self-signed certificate keypair generator

## Documentation

Execute with:

```bash
./create.sh <name>
```

The script will generate your keypair at certs/*name*

